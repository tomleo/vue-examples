#!/bin/sh

rm -r "./public-old"
mkdir -p "public-old"
mkdir -p "public"
cp "index.html" "public"

for PROJ_DIR in "transition-examples" "css-grid" "basic-transition-with-hooks-and-gsap" "nav-and-group-transitions"
do
    mv "public/$PROJ_DIR" "public-old/$PROJ_DIR"
    mkdir -p "public/$PROJ_DIR"

    cd "$PROJ_DIR"

    echo "running npm install"
    npm install --silent > "/dev/null" 2>&1

    echo "running: npm build"
    npm run build

    echo "pwd: $(pwd)"
    echo "$(ls)"

    public_dir=$(realpath "../public/$PROJ_DIR/")
    echo "public_dir: $public_dir"

    mv dist/* "$public_dir/"

    cd ../
done


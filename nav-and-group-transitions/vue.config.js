module.exports = {
    publicPath: process.env.NODE_ENV == 'production'
        ? '/vue-examples/nav-and-group-transitions/'
        : '/'
}

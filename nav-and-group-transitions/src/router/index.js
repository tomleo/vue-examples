import Vue from 'vue'
import VueRouter from 'vue-router'
import Modal from '../views/Modal.vue'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'modal',
      component: Modal
    },
    {
      path: '/list',
      name: 'list',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "list" */ '../views/List.vue')
    }, 
]

const router = new VueRouter({
  routes
})

export default router

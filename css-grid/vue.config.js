module.exports = {
    publicPath: process.env.NODE_ENV == 'production'
        ? '/vue-examples/css-grid/'
        : '/'
}

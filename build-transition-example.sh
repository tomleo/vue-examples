#!/bin/sh

PROJECT_DIR="transition-examples"

cd "$PROJECT_DIR"
npm install
npm run build
cd ../
# mkdir -p "public/$PROJECT_DIR"
mkdir -p "public"
mv "$PROJECT_DIR/dist" "public/$PROJECT_DIR"

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
    {
      path: '/simple',
      name: 'simple',
      component: () => import(/* webpackChunkName: "simple" */ '../views/Simple.vue')
    },
    {
      path: '/stagger',
      name: 'stagger',
      component: () => import(/* webpackChunkName: "stagger" */ '../views/Stagger.vue')
    }
]

const router = new VueRouter({
  routes
})

export default router

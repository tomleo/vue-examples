module.exports = {
    publicPath: process.env.NODE_ENV == 'production'
        ? '/vue-examples/basic-transition-with-hooks-and-gsap/'
        : '/'
}
